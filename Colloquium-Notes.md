# Bachelor-Verteidigung – Notizen

## Einleitung – 3

- Bluetooth ist eine langzeiterprobte Technologie → 20 jährige Entwicklungsgeschichte

- aktuell einer der wichtigsten Standards für drahtlose Kommunikation im Bereich Consumer Electronics

----

## 4

- In 2020 4 Mrd. verkaufte Geräte

- Wachstum von ca. 10 % jährlich, *Ähnlich wie mein Puls innerhalb der letzten Minuten*

- Prognose: 6,4 Mrd. verkaufte Geräte in 2025

----

## 5

- Neue Rolle seit Ausbruch der Corona-Pandemie

- Wenigstens 38 Länder nutzen BLE zur Kontaktverfolgung

----

## 6

- Trotz Wichtigkeit bisher keine Werkzeuge zur automatisierten Sicherheitsanalyse
  
  - Im Sinne eines Tools, was automatisiert die Umgebung nach Geräten scannt und die Verwundbarkeit der Geräte bewertet

----

## 7

- Machbarkeit eines solchen Werkzeugs ist Forschungsgegenstand der Arbeit

- Dazu notwendige Schritte sind:
  
  - Extraktion technischer Geräteattribute
  
  - Erlangen von Informationen über Vulnerabilitäten des Gerätes
  
  - Validierung der Präsenz der Vulnerabilitäten auf dem Gerät 

----

## 8

- Daraus Forschungsfragen:
  
  - **Q0** Is automated security-testing for Bluetooth-enabled devices practically feasible?
  - Einschränkung: BLE
    - Da Bluetooth Classic im wesentlichen abgelöst und irrelevant 

----

## 9

- Zur Beantwortung die folgenden Unterfragen:
  
  - **Q1** What technical information can **automatically** and **non-invasively** be extracted from a given device?
  
  - **Q2** What information is needed to automatically resolve a device to a vulnerability or a set of vulnerabilities?
  
  - **Q3** How can the gap between the gathered and needed information be closed automatically?
  
  - **Q4** How can it be evaluated automatically whether a potential vulnerability is present on a given device?

----

## 10

Außerdem Prämissen, die Einsatz als Pentesting-Tool ermöglichen

- Kein physischer Zugriff auf das Gerät

- Große Menge an zu testenden Geräten

- Sicherheitsanalyse läuft automatisch und ohne User-Interaktion

----

## 11

- Zur Veranschaulichung Visualisierung der Arbeitsschritte und Informationsflüsse

- Im folgenden: Eingehen auf die einzelnen Schritte bzw. Unterfragen

----

## 12

- Beim Extrahieren von Gerätedaten fällt schnell auf, dass verschiedene Level/Tiefen vorhanden sind

- Abhängig von der Tiefe sind die Daten mehr oder weniger hilfreich

- Schichtenmodell zur Darstellung eines arbiträren BLE-Geräts

- Soll Vergleichbarkeit und Vereinfachung bieten

- Alle Methoden werden auf das Schichtenmodell bezogen

- Schichten
  
  - DA1: Device Class
    
    - Gerätekategorie: Smartphone, Herzschrittmacher, Hörgerät, etc.
  
  - DA2: Manufacturer
    
    - Hersteller: Samsung, Apple, Logitech etc.
  
  - DA3: Device
    
    - Gerätename kundenfreundlich: Samsung Galaxy Tab S6 lite, Logitech MX Anywhere 2
  
  - DA4: Device Version
    
    - Detaillierte Modelnummer: SM-P610N, SM-P610, SM-P615
  
  - DA5: Software
    
    - Informationen zur verwendeten Software, z. B. Betriebssystem
  
  - DA6: Bluetooth Chipset
    
    - Verwendeter Bluetooth-Chipsatz oder SoC
  
  - DA7: Software Version
    
    - Detailliertere Informationen zur Software
  
  - DA8: Bluetooth Protocol Version
    
    - Version des verwendeten Bluetooth-Protokolls
  
  - DA9: Bluetooth Chipset Driver & Firmware Version
    
    - Informationen zum Bluetooth Chipsatz Treiber bzw. Firmware Versionen

- Modell könnte sicherlich noch erweitert oder feingranularer dargestellt werden
  
  - hat sich aber als sehr hilfreich und ausreichend genau erwiesen

----

## 13

- Verschiedene Herangehesweisen, mit deren Hilfe an verschiednenen Stellen Daten extrahiert werden können

- Grundlage für konkrete Methoden

- Scanning, Pairing, Connecting, Sniffing

----

## 14

- Scanning
  
  - Zeichnet verbindungslose Advertisement-Pakete auf
  
  - Rein passiver Ansatz
    
    - Dadurch für Advertiser nicht aufspürbar
  
  - Benötigt keine besondere Hardware
  
  - Eignet sich im Rahmen der Prämissen sehr gut

----

## 15

- Pairing
  
  - Übliche Methode für Informationsaustausch zwischen zwei Geräten
    
    - Methode zum Erstellen und Austauschen von Keys für verschlüsselte Verbindung
  
  - Benötigt in der Regel User-Interaktion auf beiden Geräten
    
    - Für Pairing; Ausnahme: "Just-Works"-Mechanismus für Geräte, die kein Display für 6-stellige Nummer oder sonstige Interaktionsmöglichkeiten bieten
    
    - Gerät muss pairable sein
  
  - Verletzt Prämissen, wenn Interaktion notwendig

----

## 16

- Connecting
  
  - != Pairing, in anderen Anwendungsbereichen synonym
  
  - Stellt unverschlüsselte Verbindung zwischen Geräten her
    
    - Stellt weniger Daten bereit als Pairing
  
  - Benötigt unter Umständen User-Interaktion
    
    - Um Gerät connectable zu machen
  
  - Nicht jedes Gerät unterstützt Conecting ohne Pairing
    
    - Geräte schließen Verbindung direkt oder versucht Upgrade auf verschlüsselte Verbindung
  
  - User-Interaktion verletzt unter Umständen Prämissen

----

## 17

- Sniffing
  
  - Folgen und Mithören des Datenaustauschs zweier weiterer Geräte
    
    - Inklusive Channel-Hopping und dynamischer Anpassung der Hopping-Parameter
  
  - Benötigt spezielle Hard- und Software
    
    - Rechenaufwand zum bestimmen der Channel-Hopping-Parameter etc.
  
  - Kann nur eine Verbindung gleichzeitig mithören
  
  - Passive Methode
  
  - Spezielle Hardware verletzt Prämissen

----

## 18

- Auswahl der Herangehensweisen
  
  - Scanning verletzt keine Prämissen → Wird genutzt
  
  - Sniffing verletzt in jedem Fall Prämissen → Wird verworfen
  
  - Pairing und Connecting verletzen nicht in jedem Fall Prämissen → Werden trotzdem genutzt

----

## 19

- Als nächstes konkrete Methoden zur Extraktion von Geräteattributen

- Device Address: Nutzt Public Device Address, um Hersteller herauszufinden
  
  - Häufiger genutzt: Random Device Address
    
    - Muss nicht bei IEEE registriert werden
    
    - Bietet Methoden zur Wahrung der Privatsphäre

- Appearance Value: Wert in den Advertisements, der die Art des Geräts spezifiziert
  
  - Wird genutzt, um z. B. passendes Icon in Pairing-Menü anzuzeigen

- Device Name: Häufig DA3 (Device) oder DA4 (Device Version) als Standard
  
  - Methode zur Klassifikation ob DA3, DA4 oder was anderes notwendig

- Identification via WiFi Chipsets: Identifiziert WLAN-Chipsatz anhand von Antwortmuster auf bestimmte fehlerhafte WLAN-Frames
  
  - Fingerprinting mit Community-gepflegter Fingerabdruckdatenbank
  
  - Ansatz funktioniert nicht mehr, da Datenbank und Tools nicht mehr verfügbar

- GATT Profile: Generic Attribute Profile
  
  - Geräteattribute werden nach pairing abrufbar
  
  - PnP ID gibt Informationen über Hersteller
  
  - Weiterhin Informationen wie Hersteller, Modellnummer, Hardwarerevision, Softwarerevision, Firmwareversion abrufbar

- Manufacturer-specific Advertisement Data: Teil der Advertisements
  
  - Hersteller implementieren eigene Protokolle via BLE Advertisements
  
  - Dafür eigener GAP service (ID 0xff)
  
  - Beinhaltet eine Company ID, welche auf Hersteller aufgelöst werden kann

----

## 20

- 6 Methoden, die insgesamt 7 DAs extrahieren können (alle außer Software und Protokoll Version), allerdings alle mit ihren eigenen Einschränkungen

----

## 21

- Methoden in Verbindung mit der genutzten Herangehensweise und ihren Einschränkungen

- Leitet über zur Antwort auf Forschungsfrage Q1

----

## 22

- Forschungsfrage Q1: „What technical information can automatically and non-invasively be extracted from a given device?“

- DA1 (Device Class), DA2 (Manufacturer), DA3 (Device), DA4 (Device Version) **problemlos**

- DA7 (Software Version), DA9 (Bluetooth Chipset & Firmware Version), solange Zielgerät „Just Works“ pairing nutzt
  
  - Pairingmethode für Geräte, die keine Möglichkeit für Anzeige einer 6-stelligen Nummer oder anderer Nutzerinteraktion haben

- DA6 eventuell, wenn die Methode noch funktionieren würde

- DA5 (Software) und DA8 (Bluetooth Protocol Version) nicht mit Methoden aus dieser Arbeit

- Im Folgenden: Beleuchten der Problemstellung aus der anderen Perspektive
  
  - Bisher: Welche Informationen haben wir? 
  
  - Jetzt: Welche Informationen brauchen wir?

----

## 23

- Informationsquellen: CPE-Datenbank, CVE-Datenbank
  
  - Größte und bekannteste Datenbanken
  
  - Daher kann von hoher Qualität der Daten ausgegangen werden
  
  - Schrittfolge
    
    1. Durchsuchen der CPE-Datenbank nach Schlüsselwort „Bluetooth“
    
    2. Filtern von Einträgen ohne CVE-Eintrag
    
    3. Abrufen von Informationen zu zugehörigen CVE-Einträgen
    
    4. Alternativer Ansatz CVE → CPE wurde aus Qualitätsgründen verworfen
       
       - Viele, wenn nicht die meisten Einträge nutzen Schwachstellen in anderen Teilen des Bluetooth Stacks und nicht im Übertragungsprotokoll an sich
       
       - Daher für automatisierten Ansatz ungeeignet

----

## 24

- Auswertung der CPE-Einträge ergibt das folgende Bild
  
  - Gleichzeitig Antwort auf Q2
  
  - DA6 und DA9 zusammen über 50 % der Einträge, gefolgt von DA5 und DA8

- Zurück erinnern an extrahierbare Attribute: Überschneidung klein

----

## 25

- Beide überschneidende Attribute haben massive Eisnchränkungen
  
  - DA6 nicht nutzbar
  
  - DA9 wahrscheinlich nicht nutzbar
    
    - Erhebungen zeigen, dass in 2019 7,4 % der Geräte ohne Authentisierung pairable waren
    
    - Zahl ist zwar hoch, aber für einen automatisierten Ansatz nicht hoch genug

----

## 26

- Ansatz nicht praktikabel für großflächiges automatisierte Sicherheitsanalyse

- Zusätzliche manuelle Schritte notwendig, um weitere Daten aus bestehenden Informationen zu gewinnen
  
  - Antwort auf Q3: „How can the gap between the gathered and needed information be closed automatically?“

----

## 27

- Zwischenfazit: Q0 Is automated security-testing for Bluetooth-enabled devices practically feasible?

- Nein, da einer der Kernaspekte die **automatisierte** Sicherheitsanalyse war

- Anpassung von Q0 für weiteren Verlauf: Ist eine Sicherheitsanalyse von Bluetooth-Geräten **überhaupt** praktikabel?

- Unter der Maßgabe der **neuen** Q0 → Vorstellung alternativer Ansätze

----

## 28

- Erneutes Betrachten des CVE-Ansatzes
  
  - Dieser wurde bisher nur unter Prämisse **automatisierte** Sicherheitsanalyse betrachtet

- Verschiedene beispielhafte Exploit-Foren und -Datenbanken
  
  - Ohne Anspruch auf Volständigkeit

- Zulassungsdokumente und -daten

----

## 29

- CVE → CPE-Schrittfolge:
  
  - Durchsuchen der CVE-Datenbank nach Schlüsselwort „Bluetooth“
  
  - Abrufen der mit den CVE-Einträgen verbundenen CPE-Einträgen
  
  - Suchen des Zielgeräts in den CPE-Einträgen

- CVE direkt
  
  - Direktes Suchen der Geräteinformationen (z. B. DA3 [Device] oder DA4 [Device Version]) in der CVE-Datenbank

- Sachbezug der Daten immer noch problematisch, jedoch bei manuellem Ansatz bei  geringer Anzahl an Zielgeräten handhabbar

----

## 30

- Exploit-Foren
  
  - Verschiedene beispielhafte Foren
  
  - "Clearnet" sowie Overlay-Netwerke (Beispielhaft Tor)
  
  - Beispiele
    
    - Raidforums.com: Früher sehr aktives Forum
      
      - Schauplatz vieler großer Datenleaks, z.B. Leak von Userdaten von 533 Mio. Facebookaccounts in 2021
      
      - Beschlagnahmt in 2022
    
    - dread: Ähnlich reddit
      
      - Bietet selbst keine sinnvollen Daten
      
      - Links zu weiteren potentiell interessanten Foren
    
    - tor.taxi: Index mit weiteren Tor-Seiten
    
    - Weitere Beispiele: Insgesamt wenig erhellend
      
      - Viele geben Inhalte nur für Mitglieder frei
      
      - Mitgliedschaft kostet teilweise Geld
      
      - Hauptproblematik: Nicht ersichtlich, ob bestimmte Inhalte nur für Mitglieder höherer Level sichtbar sind

- Exploit-Datenbank exploit-db.com
  
  - Beinhaltet ca. 45.000 Einträge → nicht besonders groß
    
    - davon ca. 40 Einträge zum Stichwort "Bluetooth"
    
    - Stichproben: Qualität der angeblichen Exploits variiert sehr stark
      
      - Textfiles mit Konsolenausgaben bis C-Implementationen von Exploits
    - Potentielle Antwort auf **Q4**

----

## 31

- Jedes Bluetooth-Gerät muss Zulassungsprozess durchlaufen, bevor es auf den Markt gebracht werden darf
  
  - Beispielhaft FCC (federal Communications Comission)
    
    - Da die meisten Geräte für den Verkauf in USA gedacht
    
    - Schrittfolge Beispielgerät "Samsung Galaxy S21"
      
      1. Suche nach "Samsung Galaxy S21 FCC filing" → zeigt Artikel auf Nachrichtenseiten
      
      2. Nachrichtenseiten linken eine Website, die einen FCC Report enthält
      
      3. Report beinhaltet Modelnummer des Smartphones sowie Modellnummer des Chipsatzes
    
    . Website fccid.io enthält Suchfunktion für Geräte → ergibt FCC ID
    
    . Chipsatz-Modellnummer vermutlich in Mobilfunk-Reports auffindbar
    
    . Hoher manueller Aufwand
    
    . Funktioniert unter Umständen nicht für z. B. unpopuläre Geräte
    
    . Wenn Ansatz funktioniert, schließt er Lücke zwischen DA3 (Device)/DA4 (Device Version) und DA6 (Bluetooth Chipset) → Antwort auf **Q3** mit manuellem Aufwand

----

## 32

- Während Arbeit mit Spezifikation: Einige Stellen, an denen Steganographie möglich wäre

- Kategorisierung nach Wendzel et al. 2022

- Network Reserved/Unused State/Value (ID E1.n1n.)
  
  - PnP ID – Vendor ID Source
    
    - Werte 0 und 3 bis 255 reserviert
    
    - Detektierbar via Sniffing
  
  - Extendes Advertising Report – Event Type
    
    - 9 reservierte Bits
    
    - Detektierbar via Scanning
  
  - PnP ID – Vendor ID
    
    - Nicht zugewiesene/private Werte – Anzahl unbekannt
    
    - Detektierbar via Sniffing

----

## 33

- Network State/Value Modulation (ID E1n1)
  
  - PnP ID – Product ID/Product Version
    
    - Je 2 Oktette
    
    - Undetektierbar, da Wertezuweisung nicht öffentlich einsehbar
  
  - GATT Characteristics
    
    - Die meisten GATT Charakteristiken können ohne erregen eines Verdachts geändert werden
    
    - Kapazität hängt von Anzahl und Art der Charakteristiken ab

----

## 34

- Automatisierte Sicherheitsanalyse ist mit den Methoden aus dieser Arbeit nicht möglich

- Mit aufgeweichten Prämissen (keine Automatisierung notwendig)
  
  - Q3 (How can the gap between the gathered and needed information be closed?) wird durch FCC Zulassungsdokumente o. ä. beantwortet
  
  - Q4 (How can it be evaluated whether a potential vulnerability is present on a given device?) wird durch exploit-db.com oder o. ä. beantwortet

- Gesamtfazit: Sicherheitsanalyse ist mit manuellen Schritten (und ggf. Einschränkungen) möglich

----

## 35

- Metrik für Matching-Wahrscheinlichkeit
  
  - Modell zur Berechnung der Wahrscheinlichkeit, dass eine identifizierte Anfälligkeiten tatsächlich auf einem Gerät vorhanden ist

- Exploitation von bekannten Anfälligkeiten
  
  - Sammeln und Bewerten von Exploits oder von Exploitsammlungen

- Arbeit an Automatisierbarkeit
  
  - Viele Aspekte, die zu der Erkenntnis beigetragen haben, dass automatisiertes Testen nicht möglich ist.
