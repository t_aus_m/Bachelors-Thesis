% !TeX spellcheck = en_GB
\chapter{Background\label{ch:background}}
This \namecref{ch:background} will focus on creating a solid base for the understanding of the rest of the thesis by giving background information on the most important technical fields regarding this thesis. This will be split in three parts: First the \Cref{sec:bl-classic,sec:ble} will elaborate on Bluetooth, then \Cref{sec:cve,sec:cpe,sec:cvss} will cover the basics about the used databases. Lastly \Cref{ch:related_work} will give an overview of the research that has been done in related fields.
\section{Bluetooth\label{sec:bl-classic}}
Bluetooth is a standard for wireless communication which has been developed since 1989~\cite{tuanc.nguyen_WhoInventedBluetooth_2021} and that works on the 2.4 GHz unlicensed industrial, scientific, and medical (ISM) frequency band. It is a low-powered, low-bandwidth protocol and as such is mostly used for mobile application areas~\cite{bluetoothsiginc._BluetoothTechnologyOverview_2022}. Bluetooth is being developed and managed by the Bluetooth special interest group (SIG). The first release of the specification (version 1.0) was released in 1999~\cite{bluetoothsiginc._OurHistoryBluetooth_2018}, the most current revision of the standard is version 5.3, released on \DTMdate{2021-07-13}~\cite{bluetoothsiginc._SpecificationsBluetoothTechnology_2022}.

Since the specification of Bluetooth 4.0 in 2009, the legacy Bluetooth protocols are referred to as Bluetooth Classic. These protocols have been renamed in favour of the new Bluetooth stack, which is called Bluetooth Low Energy (Bluetooth LE or BLE)~\cite{bluetoothsiginc._OurHistoryBluetooth_2018}. Since the release of BLE, more and more use cases are being shifted from using Bluetooth Classic to using BLE.

Bluetooth Classic is a purely connection-oriented (synchronous or asynchronous) protocol that is only able to form point-to-point networks. It uses 79 channels with a spacing of 1 MHz and features frequency hopping to minimise interferences and also chances of interception. As of today, the only two mature application areas left are audio streaming and data transfer, with audio streaming support for BLE being announced to launch in the near future~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}. All in all, the relevance of Bluetooth Classic has been declining for the last few years~\cite{bluetoothsiginc._BluetoothTechnologyOverview_2022}. Because of the loss in significance and the limited possibility of capturing data due to channel hopping, Bluetooth Classic will not be covered in this thesis.


\section{Bluetooth Low Energy — BLE\label{sec:ble}}
As mentioned before, since its release in 2009, Bluetooth Low Energy has been steadily gaining in importance. As the name suggests, it consumes less energy than Bluetooth Classic, while also offering a larger feature set. BLE supports broadcast and mesh networking in addition to the point-to-point networks possible in Bluetooth Classic. Also, in addition to the connection-oriented synchronous and asynchronous data transports, there is now the possibility to transmit data connectionless and isochronous.

BLE supports 40 channels with a spacing of 2 MHz, three of which are dedicated to advertisements (connectionless data transmission). The ability to use advertisements makes BLE suitable for all kinds of Internet of things (IoT) applications, such as presence tracking, proximity tracking, directional tracking and, in the near future, also distance estimation~\cite{bluetoothsiginc._BluetoothTechnologyOverview_2022}. Most Bluetooth-based approaches to tracking or tracing contacts during the COVID-19 pandemic mentioned above use the advertisement feature of BLE, underlining the importance of the technology even further~\cite{mishaalrahman_ListCountriesUsing_2021}.

\subsection{Addressing a Device\label{sec:addressing}}
Bluetooth offers multiple ways to address a device. On the one hand there is the Public Device Address, on the other hand there are different options to define Random Device Addresses, which are only available via Bluetooth Low Energy, however. The different methods of defining a Device Address and how they are related to one another can be seen in \Cref{fig:bluetooth-addresses}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/Bluetooth-addresses.pdf}
    \caption[Different ways to address a BLE device]{There are multiple ways to address a BLE device. It is mandatory to either set a Public or Static Random Device Address. The address types not covered in this thesis are coloured grey.}
    \label{fig:bluetooth-addresses}
\end{figure}

Per its specification, a Device Address has a length of 48 bits. It is mandatory to either provide a Public Device Address or a Static Random Device Address. The other types of Random Device Addresses are optional and are used for privacy-retaining purposes. They also require to be paired to the device, thus not corresponding to the premises set in \Cref{sec:research-question}, and are therefore not discussed any further in this thesis.

The Static Random Device Address is the more common type of Device Address, since it does not require any registration and there is no fee associated with acquiring the Device Address. It can either be fixed or generated at the time of boot. The Static Random Device Address consists of the two most significant bits being 1 and the remaining 46 bits being random, with at least one bit of the random part being 1 and at least one being 0, as can be seen in \Cref{fig:static-random-device-address}. Due to its complete randomness, the Static Random Device Address offers no informational gain towards answering the research questions of this thesis.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/Static-Random-Device-Address.pdf}
    \caption[The structure of the Static Random Device Address]{The two most significant bits of the Static Random Device Address must be 1; of the remaining 46 randomly generated bits, at least one has to be 1 and one has to be 0~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}.}
    \label{fig:static-random-device-address}
\end{figure}

The other type of mandatory Device Address is the Public Device Address. As mentioned earlier, this type of Device Address is less common than the Static Random Device Address, because it requires registration with the IEEE\footnote{\url{https://www.ieee.org/}} in the form of an EUI-48 (extended unique identifier with a length of 48 bits)~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}. Of these 48 bits, the first two are used as flags that are not relevant for this topic. The next at least 22 and at most 36 bits are assigned by the IEEE to identify the manufacturer of a device via the so-called Organisational Unique Identifier (OID). The remaining 24 to 12 bits are available to be set by the entity referenced by the OID. A representation of the structure of the Public Device Address can be found in \Cref{fig:mac}~\cite{ieee_IEEEStandardLocal_2014}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/MAC.pdf}
    \caption[The structure of the Public Device Address]{The amount of organisation-assignable IDs depends on the length of the OID~\cite{ieee_IEEEStandardLocal_2014}.}
    \label{fig:mac}
\end{figure}

\subsection{Generic Attribute Profile — GATT\label{sec:background:gatt-profile}}
Another important part of the Bluetooth Low Energy specification is the Generic Attribute (GATT) Profile. It is based on the Attribute Protocol (ATT), which in turn is used to allow a device that acts as a server to expose certain characteristics and their respective value to client devices.

The GATT Profile defines a framework that provides procedures and formats of services and their characteristics. The procedures defined include discovering, reading, writing, notifying and indicating characteristics, as well as configuring the broadcast of characteristics. In general, these values can only be accessed by devices connected to the server. To read information from a device connectionlessly, the GAP service (see \Cref{sec:background:gap-profile}) has to be used.

The data served by the GATT profile is stored in Attributes on the server device according to the Attribute Profile. An Attribute consists of four parts, which can be seen in \Cref{fig:att-attribute}.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/ATT-Attribute.pdf}
    \caption[The different parts of a GATT Attribute]{An Attribute consists of four parts: Attribute Handle, Attribute Type, Attribute Value and Attribute Permissions~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}.}
    \label{fig:att-attribute}
\end{figure}

The four parts of an Attribute are:
\begin{description}
    \item[Attribute Handle] The Attribute Handle is an server-defined index corresponding to a specific Attribute.
    \item[Attribute Type] The Attribute Type is a UUID that describes the Attribute Value.
    \item[Attribute Value] The Attribute Value is the data described by the Attribute Type and indexed by the Attribute Handle.
    \item[Attribute Permissions] Attribute Permissions cannot be read or written by a client, they are only used by the server to determine whether read and/or write access should be granted to a client~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}.
\end{description}
%- Datenstruktur nach S. 1472

\subsection{Generic Access Profile — GAP\label{sec:background:gap-profile}}
Contrary to the Generic Attribute Profile described in \Cref{sec:background:gatt-profile}, the Generic Access Profile (GAP) is used to provide data of the broadcaster to an observer in a connectionless way. The GAP is used e. g. for broadcasting the name, appearance, discoverability, pairability, and many other attributes. This information is used, for instance, for showing a fitting icon beneath the name of a device during the pairing process. Additionally, arbitrary data for vendor/manufacturer-specific subjects can be broadcasted~\cite{bluetoothsiginc._CoreSpecificationBluetooth_2021}. One possible application for this is the Google-Apple-Exposure-Notification (GAEN) framework used to trace contacts during the COVID-19 pandemic~\cite{mishaalrahman_ListCountriesUsing_2021}. The available attributes are defined in the “Generic Access Profile Assigned Numbers” document.\footnote{\url{https://btprodspecificationrefs.blob.core.windows.net/assigned-numbers/Assigned\%20Number\%20Types/Generic\%20Access\%20Profile.pdf}}

\section{Common Vulnerabilities and Exposure — CVE\label{sec:cve}}
CVE stands for Common Vulnerabilities and Exposure. It is a system that aims to catalogue and define publicly disclosed vulnerabilities in such a way that they can easily be referenced. Vulnerabilities are discovered and defined by CVE Numbering Authorities (CNAs) based on a well-defined set of rules\footnote{\url{https://www.cve.org/ResourcesSupport/AllResources/CNARules}} to ensure that the description of vulnerabilities is consistent~\cite{themitrecorporation_OverviewCVE_2021}.

Each vulnerability is assigned a CVE Record, which contains a CVE ID as well as other details such as:
\begin{itemize}
    \item affected product(s) (as definded by the CPE, see \Cref{sec:cpe})
    \item affected or fixed product version (as definded by the CPE, see \Cref{sec:cpe})
    \item vulnerability type
    \item root cause
    \item impact (as defined by the CVSS, see \Cref{sec:cvss})
    \item at least one public reference.
\end{itemize}
The CVE ID is structured as follows:
\begin{Verbatim}
    CVE-<year>-<arbitrary digits>
\end{Verbatim}
The individual components of the CVE ID can be described as such:
\begin{itemize}
    \item \verb|<year>| is either the year that the CVE ID was reserved or the year that it was made public. It is, however, never used to indicate the year that the vulnerability was discovered.
    \item \verb|<arbitrary digits>| is a sequence number with four or more digits. There is no upper limit to the number of digits~\cite{themitrecorporation_ProcessCVE_2021}.
\end{itemize}

One example for a CVE is \verb|CVE-2021-39774|\footnote{\url{https://nvd.nist.gov/vuln/detail/CVE-2021-39774}}, a vulnerability that was assigned in 2021. Its affected products are denoted as defined by the CPE (see \Cref{sec:cpe}) as \verb|cpe:2.3:o:google:android:12.0:*:*:*:*:*:*:*|, the vulnerability type is defined by the CWE\footnote{\url{https://cwe.mitre.org/}} — which is not covered any further in this thesis — and denotes \verb|CWE-125|, which denotes an out-of-bounds read. The root cause is given in the description of the vulnerability: For this example, it is a “missing bounds check”. The impact is evaluated as defined by the CVSS (see \Cref{sec:cvss}) with a vector of \verb|CVSS:3.1/AV:L/AC:L/PR:L/UI:N/S:U/C:N/I:N/A:H| and a Base Score of \verb|5.5 Medium|. The given reference is \url{https://source.android.com/security/bulletin/android-12l}.

\section{Common Platform Enumeration — CPE\label{sec:cpe}}
The Common Platform Enumeration (CPE) is a method that is standardised by the National Institute of Standards and Technology (NIST).\footnote{\url{https://www.nist.gov/}} It is used to formally describe and identify classes of applications, operating systems and hardware devices. The current version of the CPE is 2.3.

CPE consists of several specifications, one of which — the CPE Naming specification — defines well-formed CPE names (WFNs) as an abstract logical construction as well as procedures to represent WFNs in a machine-readable encoding. To give an example, the following WFN represents Microsoft Internet Explorer 8.0.6001 Beta:
\newcommand{\comma}{,}
\begin{Verbatim}[breaklines=true, breakafter=\comma]
    wfn:[part="a",vendor="microsoft",product="internet_explorer",version="8\.0\.6001",update="beta"]
\end{Verbatim}
Another defined binding is called the formatted string binding. While the syntax is different, it also supports additional product attributes and is moreover the binding used to query the CPE database. The example above represented as a formatted string binding would be the following:
\begin{Verbatim}[breaklines=true, breakafter=:]
    cpe:2.3:a:microsoft:internet_explorer:8.0.6001:beta:*:*:*:*:*:*
\end{Verbatim}
The more general structure of the formatted string binding would be the following:
\begin{Verbatim}[breaklines=true, breakafter=:]
    cpe:<cpe_version>:<part>:<vendor>:<product>:<version>:<update>:<edition>:<language>:<sw_edition>:<target_sw>:<target_hw>:<other>
\end{Verbatim}
The individual components of the formatted string can be described as follows:
\begin{itemize}
    \item a wildcard denoted by \verb|*| can be put at every position
    \item \verb|<cpe_version>| defines the CPE version
    \item \verb|<part>| can be one of three values:
    \begin{itemize}
        \item \verb|a| determines that the CPE is \textbf{application}-specific
        \item \verb|h| determines that the CPE is \textbf{hardware}-specific
        \item \verb|o| determines that the CPE is \textbf{operating system}-specific
    \end{itemize}
    \item \verb|<vendor>| defines the vendor of the affected product
    \item \verb|<product>| defindes the name of the affected product
    \item \verb|<version>| defines a specific version of the product that is affected
    \item \verb|<update>| defines a specific minor version/point release of the product that is affected
    \item \verb|<edition>| defines further restrictions to the affected version
    \item \verb|<language>| defines the supported language of the UI of the product
    \item \verb|<sw_edition>| defines how the product is tailored to a specific market or target group
    \item \verb|<target_sw>| defines the software environment in which the product operates
    \item \verb|<target_hw>| defines instruction sets (e. g. x86) or bytecode-intermediate languages
    \item \verb|<other>| defines any other describing or identifying properties~\cite{cheikes_CommonPlatformEnumeration_2011}.
\end{itemize}
\section{Common Vulnerability Scoring System — CVSS\label{sec:cvss}}
The Common Vulnerability Scoring System (CVSS) is an open framework used to assign a numerical score reflecting the severity of a vulnerability. The current revision of the CVSS is version 3.1, which is focused on clarifying and improving the CVSS v3.0 standard. Since March 2016, CVSS v3.0 is an international standard for scoring vulnerabilities adopted by the International Communications Union (ITU)\footnote{\href{https://www.itu.int/rec/T-REC-X.1521-201603-I/en}{\url{https://www.itu.int/rec/T-REC-X.1521-201603-I/en}}}~\cite{forumofincidentresponseandsecurityteamsinc._CVSSV3User_2021}.

“CVSS consists of three metric groups: Base, Temporal, and Environmental. The Base group represents the intrinsic qualities of a vulnerability that are constant over time and across user environments, the Temporal group reflects the characteristics of a vulnerability that change over time, and the Environmental group represents the characteristics of a vulnerability that are unique to a user's environment. The Base metrics produce a score ranging from 0 to 10, which can then be modified by scoring the Temporal and Environmental metrics.”~\cite{forumofincidentresponseandsecurityteamsinc._CVSSV3Specification_2021}

The three metric groups in turn consist of different metrics which, if combined, form the score of the according metric group, as can be seen in \Cref{fig:cvss-metrics}. The score, that is then calculated based on the values of the different metrics, is a number between 0 and 10 with a maximum of one decimal place. All numeric scores can also be mapped to a textual representation, the mapping of which can be seen in \Cref{tab:cvss-mapping}~\cite{forumofincidentresponseandsecurityteamsinc._CVSSV3Specification_2021}.

\begin{table}[H]
\centering
\begin{tabular}{|l|l|}
\hline
\textbf{rating} & \textbf{CVSS score} \\ \hline
none            & 0.0                 \\ \hline
low             & 0.1 – 3.9           \\ \hline
medium          & 4.0 – 6.9           \\ \hline
high            & 7.0 – 8.9           \\ \hline
critical        & 9.0 – 10.0          \\ \hline
\end{tabular}
\caption[The mapping of numeric CVSS scores to their textual representation]{Numeric CVSS scores can be mapped to textual qualitative ratings~\cite{forumofincidentresponseandsecurityteamsinc._CVSSV3Specification_2021}.}
\label{tab:cvss-mapping}
\end{table}

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/CVSS-metrics.pdf}
    \caption[The different metric groups of the CVSS]{The CVSS consists of three metric groups: Base, Temporal and Environmental, of which each consists of a set of metrics~\cite{forumofincidentresponseandsecurityteamsinc._CVSSV3Specification_2021}.}
    \label{fig:cvss-metrics}
\end{figure}