% !TeX spellcheck = en_GB
\chapter{Evaluation\label{ch:evaluation}}
After \Cref{ch:methodology} covered the methodology of extracting device attributes and looking up vulnerabilities separately, answering \ref{Q1} and \ref{Q2}, this \namecref{ch:evaluation} will connect both tasks and evaluate the possibilities for estimating the vulnerability of a device based on that. This will also serve to answer \ref{Q3}. Additionally, the feasibility of automatically testing a device for found vulnerabilities will be evaluated, answering \ref{Q4}.

\section{CPE Approach\label{sec:eval-cpe}}

If the data from \Cref{sec:lookup-conclusion} is compared to the information that can be extracted from devices (see \Cref{tab:methods} on \cpageref{tab:methods}) as well as the limitations of the respective methods used (see \Cref{tab:extraction-approaches-comparison} on \cpageref{tab:extraction-approaches-comparison} and \Cref{sec:extraction-conclusion}), and condensed into \Cref{tab:da-matching}, it can easily be seen that the intersection between the available and needed Device Attributes is not big and both intersecting items have limitations that make them either completely unusable or at least most likely unusable. The data gathered by \Citeauthor*{zuo_AutomaticFingerprintingVulnerable_2019}~\cite{zuo_AutomaticFingerprintingVulnerable_2019} suggests that only 7.4~ \% of devices could be paired without authentication back in 2019, showing that the chances of large-scale usage of \ref{DA:chipset-driver-firmware} are rather small.

\begin{table}[H]
	\centering
	\begin{tabularx}{\textwidth}{|X|X|l|}
		\hline
		\textbf{\makecell[l]{DA\\available}} & \textbf{\makecell[l]{DA\\needed}} & \textbf{Limitations}\\\hline
		\ref{DA:device-class} &  &  \\\hline
		\ref{DA:manufacturer} &  &  \\\hline
		\ref{DA:device} &  &  \\\hline
		\ref{DA:device-version} &  &  \\\hline
		& \ref{DA:software} & \\\hline
		\ref{DA:bluetooth-chipset} & \ref{DA:bluetooth-chipset} & does not work anymore, requires device with BLE and WiFi \\\hline
		\ref{DA:software-version} &  &  \\\hline
		& \ref{DA:bluetooth-protocol} & \\\hline
		\ref{DA:chipset-driver-firmware} & \ref{DA:chipset-driver-firmware} & requires pairing \\\hline
	\end{tabularx}
	\caption[Comparison between available and needed Device Attributes along with the limitations of the method]{This table compares the extractable device attributes (“DA available”) with the attributes that are used best for querying the CPE database (“DA needed”). If there is a match between the two, the limitations of the method are listed.}
	\label{tab:da-matching}
\end{table}

The conclusion that has to be drawn from such results is that this approach is not practically feasible for usage for large-scale automated security-testing of Bluetooth devices without taking extra steps to manually gain knowledge about further device characteristics based on the obtained data. This in and by itself would already answer \ref{Q3}, which would make \ref{Q4} obsolete and therefore answer \ref{Q0} with a clear “no”, since one of the key aspects of this research was to check whether \textit{automated} security-testing was possible, which — as explained above — does not work.

A useful implementation as a proof of concept — as contemplated in the conceptualisation phase of this thesis — is thus no longer meaningful either as it would not really serve any purpose that other tools, especially bettercap (see \Cref{sec:bettercap}) do not already cover.

 However, there is still the option of altering \ref{Q0} by crossing out \textit{automated}. To answer the altered question if security-testing for Bluetooth-enabled devices is practically feasible (at all), some manual work is now necessary to squeeze some valuable results out of the gathered data. Several approaches — without claiming completeness — will be discussed in the following \namecref{sec:eval-alternatives}.

\section{Alternative Approaches\label{sec:eval-alternatives}}

As explained above, the complete fulfilment of the research question is not possible because there is no way with the conceived concept to fully automate the security-testing procedure. However, if the research question is altered and \textit{automated} is thrown out, there may still be potential. Therefore, this \namecref{sec:eval-alternatives} will look at and discuss alternative approaches to the main concept of this thesis using OSINT\footnote{Open-Source Intelligence} methodology that might still reach the goal of security-testing a Bluetooth device.

The issue with all of the following approaches is that they cannot be easily evaluated for their effectiveness as long as no device that is known to be vulnerable is available for testing. Without a known-vulnerable device, all claims towards effectiveness are based on assumptions that build upon a backwards search similar to the one performed in \Cref{sec:vulnerability-lookup}. Thus, no universally valid statement regarding the practicability of the approaches is possible.

\subsection{CVE Database\label{sec:alternative-CVE}}
\subsubsection{CVE → CPE}
This approach has already been considered in \Cref{sec:lookup-cve-cpe} and found unusable for automatic testing. However, for manual information retrieval it might still be fitting. As the concept of this approach has already been described in \Cref{sec:lookup-cve-cpe}, it will not be repeated here in detail.

The basic approach of this method is that all the CVE entries returned for the keyword “Bluetooth” are searched for their associated CPE entries and the information gathered from the device is checked against these CPE entries.

As mentioned in \Cref{sec:lookup-cve-cpe}, the quality of the returned results might vary greatly, though for one specific device, the number of returned results should be small enough to check manually for validity and relevance.\todo{Beispiele}

\subsubsection{CVE direcly}

Another approach based on the CVE database would be to use its contents directly without considering the CPE database at all. Every information extracted from the device, e. g. \ref{DA:device} (Device) or \ref{DA:device-version}, could be searched for in the CVE database, although this approach may and most certainly will also deliver results that are not related to Bluetooth in any way.\todo{Beispiele}

\subsection{Exploit Forums and Databases\label{sec:exploit-forums}}

In addition to the sources of information already known and used, many alternative sources exist. Some of them will be considered in the following passage.

One way that might help reaching the goal of finding out whether a device is vulnerable is to search for the device in exploit databases or forums of the hacking community. There are many places online that exist to share knowledge regarding IT security. Depending on the legality of the offered data (concerning source and pursued goal of the offerer), they might, however, be hard to find. Some of these communities go as far as to only offer access to their services using anonymisation tools and overlay networks like Tor\footnote{\url{https://www.torproject.org/}} to protect their users as well as their providers from authorities. Regardless of these communities intentions, recent history has shown that the increased need for safety, security and privacy is justified.

This \namecref{sec:exploit-forums} will try to find some platforms that are actively used and assess their value for reaching the goal of this thesis.

\subsubsection{raidforums.com}

The forum raidforums.com\footnote{\url{https://raidforums.com}} has previously been known to be one of the most active forums for blackhat hacking available in the Clearnet\footnote{The term Clearnet describes the publicly accessible Internet excluding parts that are only accessible via e. g. Tor.}. Information was in many cases available fee-based. The forum was home to some of the larger data leaks in the history of the Internet, for example the leak of the personal data of 533 million Facebook users in 2021\footnote{\url{https://www.businessinsider.com/stolen-data-of-533-million-facebook-users-leaked-online-2021-4}}. The forum and its contents have been seized on \DTMDate{2022-02-25}\footnote{\url{https://webz.io/dwp/raidforum-is-seized-where-will-its-users-go-now/}}. Thus it is not valuable for the goals of this thesis anymore. However, following the trail its users pursued might lead to other forums that took the place of \href{https://raidforums.com/}{raidforums.com}.

\subsubsection{dread}

One of the best-known and most used forums in the so-called Hidden Web\footnote{the part of the Internet that is only available through services like Tor} is dread\footnote{\url{http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion}}. It is built very much like the Clearnet site reddit\footnote{\url{https://reddit.com}} and thus offers a platform for very many heterogeneous communities. While the site itself does not seem to offer any meaningful information that would directly contribute to the topic of this thesis, it does offers the URLs of potentially interesting other sites that might provide some valuable information. Since those sites are mostly part of the Tor Hidden Web, their domains look very different from the usual URLs of the Clearnet. The TLD\footnote{top level domain} of Hidden Web Services, for example, is always \verb|.onion|. dread itself is reachable at the rather bulky URL \url{http://dreadytofatroptsdj6io7l3xptbet6onoyno2yv7jicoxknyazubrad.onion}. Some of the linked sites will be listed in the following sub-sections.

\subsubsection{tor.taxi}

tor.taxi\footnote{\url{http://tortaxi7axhn2fv4j475a6blv7vwjtpieokolfnojwvkhsnj7sgctkqd.onion}} itself is just an index of other sites, thus offering no information other than the URLs of Hidden Web Services, including but not limited to several markets, forums and search engines.

\subsubsection{CryptBB}

The forum CryptBB\footnote{\url{http://cryptbbtg65gibadeeo2awe3j7s6evg7eklserehqr4w4e2bis5tebid.onion}} is only accessible via Tor Hidden Services. None of its contents are available without being logged in. Right after registration, the user has access to a few public topics and categories. A quick search shows only a few threads related to exploits and the like, but nothing related to Bluetooth. It is still possible that there are more (fitting) topics that are not posted publicly and to which a user needs to be granted access either manually or by some kind of levelling system.

\subsubsection{XSS.is}

This forum seems to be available both in the Clearnet\footnote{\url{https://xss.is}} as well as a Tor Hidden Service\footnote{\url{http://xssforumv3isucukbxhdhwz67hoa5e2voakcfkuieq4ch257vsburuid.onion}}. It also does not expose any contents without being logged in, even the language of the interface cannot be changed without being logged in. Unfortunately, registration was not possible during testing for unapparent reasons, thus the usefulness of the forum for this thesis’ purposes could not be examined.

\subsubsection{cracked.io}

Another forum only accessible via the Clearnet is cracked.io\footnote{\url{https://cracked.io}}. According to their own statement, the forum provides “cracking tutorials, tools, leaks, marketplace and much more stuff” \Cite{cracked.io_CrackedIoLimits_2022}. Their contents are available without login, but the search function is only available while being logged in. After logging in the search quickly reveals that the forum might not live up to its claims. At least when searching for “Bluetooth Exploit”, the returned threads are either irrelevant or — even worse — try to scam the user. As considered before it is possible that the forum has relevant information to offer that are only accessible for upgraded members — the forum offers multiple paid membership options — however, none of these options could be tested.

\subsubsection{exploit.in}

exploit.in\footnote{\url{https://exploitivzcm5dawzhe6c32bbylyggbjvh5dyvsvb5lkuz5ptmunkmqd.onion}} is another forum that makes its content only available for registered users. Additionally, there are only two ways of becoming a member: the first one is to pay \$100 for the membership, the second one is by getting the free membership that is only available for people who have been active on other Hidden Web forums for a long time and have certain skills that are beneficial for the purposes of the forum (like expert-level knowledge of certain programming languages or in certain cyber security fields). This kind of exclusivity can be a hint that the community is rather active so that measures had to be taken to keep the moderation efforts manageable, which in turn may mean that there is valuable information on the forum. However, these speculations could no be confirmed nor denied since access to the forum was not possible. It also means that this source of information is equally unattainable for most other people.

\subsubsection{blackhatworld.com}

The forum blackhatworld.com\footnote{\url{https://www.blackhatworld.com}} seems to be only accessible via Clearnet and also opens up information for visitors that are not logged in. A search in the available content shows that there seems to be no publicly available helpful content regarding the questions of this thesis. Also, the forum advertises itself as “the global forum and marketplace for cutting edge [sic] digital marketing techniques and methods to help you make money in digital marketing today”, which indicates a rather different focus. The perception of the forum does not change after being logged in since the search results return the same entries as before, so that it seems like this forum is not of use either.

\subsubsection{exploit-db.com\label{sec:exploit-db.com}}

Contrary to the other mentioned sites, exploit-db.com\footnote{\url{https://www.exploit-db.com}} is an actual exploit database. With roughly 45,000 entries it is not very large, but it offers the exploits itself as well as information about the type, the platform and a status whether the exploit has been verified or not. All of that is done free of charge with no login required.

When searching the database for the terms used before (i.~e. “BLE”, “Bluetooth”, “Bluetooth LE” and “Bluetooth Low Energy”), the database returns 618, 40, 27 and 0 entries. Again the entries returned for “BLE” are mostly unrelated to Bluetooth, hence this term is disregarded. The number of results is certainly not enough to do large-scale testing based on the data of this database. Additionally, when examining some examples, it becomes obvious very fast that the quality of alleged exploits varies greatly from text files just containing console outputs to supposedly fully working implementations written in C or the like.

\subsection{Qualification Data\label{sec:alternative-qualification-data}}

As mentioned on the Bluetooth SIGs website\footnote{\url{https://www.bluetooth.com/develop-with-bluetooth/qualification-listing/}}, all devices that ship with Bluetooth capabilities have to be tested and qualified before they can be brought to market. In theory, this means that every phone, every smartwatch, every laptop and so on has to be registered, tested and qualified. This might open up some possibilities for retrieving technical device properties about a certain device directly from the licensing documentation, as some authorities might require the documentation to be publicly available.

As an example and because most devices are intended for sale in the USA, the Federal Communications Commission (FCC), which is the licensing authority of the USA, will be looked at:

A quick initial test shows that an online search for “Samsung Galaxy S21 FCC filing” brings up news articles\footnote{\url{https://www.androidauthority.com/samsung-galaxy-s21-fcc-1184233/}} that mention the FCC filing as part of their speculations about the — back then — upcoming release of the phone. It also links to a website\footnote{\url{https://fccid.io/A3LSMG991U/Test-Report/Test-Report-20201207-v1-SM-G991U-mmWave-Test-Report-Rev03-Part1-5031452}} that lists a “FCC 5 mmWave REPORT” for a mobile phone with the model number \verb|SM-G991U|. Additionally (on page 5), the chipset of the phone is mentioned to be the \verb|SM8350|, which is the model number of Qualcomms “Snapdragon 888 5G” processor. This is in fact the one found in that particular model of the “Galaxy S21 5G”.

The website\footnote{\url{https://fccid.io/search.php}} that is listed in this news article turns out to be quite an useful tool, as it is not only able to search the FCC database by ID, but also the databases of the CMIIT\footnote{licensing authority of China; China Ministry of Industry and Information Technology}, the KCC/MSIP\footnote{licensing authority of South Korea; Korean Communications Commission, now known as Ministry of Science, ICT \& Future Planning} and ANATEL\footnote{licensing authority of Brazil; Agency of National Telecommunications}. The site also features a search function that can be used to retrieve the IDs of a product. When applying the example from above and searching for “Galaxy S21”, the search reliably returns the FCC ID \verb|A3LSMG991U|, which — if in turn inserted in the FCC ID search function — returns a large page containing every application, all operating frequencies, every exhibit and the resulting grants available for that specific ID.

The number of available documents will certainly vary depending on the device and its technical properties, so it seems like there is no document that can be assumed to find the model number of the system on chip (SoC) in. However, since the SoC generally contains the modem as well, which is responsible for the cellular connection, the test reports for the mobile communications (UMTS/3G,LTE/4G,5G) might be a good place to start.

On the downside — like every other approach —, this method also has its limitations: For devices that are not licensed in a country that requires the licensing data to be publicly available, or which are simply not licensed at all, this approach will not offer any more details. Additionally, it may be complicated to find the FCC ID (or the IDs for any other licensing authority) for a specific device — especially for less common ones. And of course this approach comes with a rather large amount of manual work required to gather useful information.

However, for devices that this approach works for, the knowledge that can be gathered is really helpful, as it allows closing the gap between the data that can be extracted from the device (mostly by leveraging \ref{DA:device} and \ref{DA:device-version}) and the information that is needed to use databases like the CPE (see \Cref{sec:cpe-cve}) by offering knowledge about \ref{DA:bluetooth-chipset}. Thus, this approach also offers an answer to \ref{Q3} with the limitation that (potentially a lot of) manual labour is needed to derive needed data from the available data.

\subsection{Conclusion}

The conclusion for this \namecref{sec:eval-alternatives} draws a mixed picture: while the CVE database (see \Cref{sec:alternative-CVE}) might deliver usable results, many of the examined exploit forums failed to deliver the desired knowledge gain, with the one notable exception being exploit-db.org (see \Cref{sec:exploit-db.com}). As the number of evaluated forums and websites as well as the time spent on said sites is rather low, the probability is very high that other sites exist which offer more valuable knowledge. Also, the possibility cannot be excluded that some of the visited sites offer relevant information that can only be found by investing more resources (time and money).

On the other side, the approach using qualification data (see \Cref{sec:alternative-qualification-data}) has turned out to be a very helpful additional resource for gaining information about a device. Although significant manual labour might be needed in order to get the information that is needed, the reward is knowledge about the chipset of a device, which is one of the most valuable pieces of information regarding the CPE approach (see \Cref{sec:vulnerability-lookup}).



\section{Potential for Steganography\label{sec:eval-steganography}}
During the work on this thesis there have been several findings that indicate potential for steganography and covert channels that may be worth investigating further on. While the findings will not be illuminated to their full potential, the goal of this section is to nevertheless provide an overview that can be built upon in future research. It is worth noting that the following listing does not come with any claim to completeness. Additionally, all the mentioned covert channels could be combined or supplemented by other measures. The implications of such actions on detectability and storage will not be discussed in this thesis.

In the following sub-sections, some of the found possibilities for covert channels will be presented, categorised by the pattern they use according to \cite{wendzel_GenericTaxonomySteganography_2022}.

\subsection{Network Reserved/Unused State/Value\label{sec:stego-reserved}}
As described in the taxonomy by \Citeauthor{wendzel_GenericTaxonomySteganography_2022} \Cite{wendzel_GenericTaxonomySteganography_2022}, the use of a reserved/unused state/value is assigned the ID \verb|E1.n1n.|.
\begin{itemize}
	\item Inside the PnP ID (see \Cref{sec:pnp-id}), which is part of the GATT characterisics, the Vendor ID Source has values that are reserved for future use. The only currently used values are 1 and 2, thus the value 0 and the values 3 to 255 are available for use as a covert channel. The channel should be rather simple to detect as there are (currently) no versions of the Bluetooth standard that implement more values for the Vendor ID source. However, a typical Bluetooth device that has no access to the Internet itself (like e. g. a fitness tracker) has no way of knowing all the Bluetooth standards in existence, since for that it would need a lot of storage and would need to be updated every time a new version of the standard is released. Thus, offline-detection might prove to be hard to perform, if not impossible.
	\item The \verb|Event Type| inside the Extended Advertising Report (see \Cref{app:ble-advertisement}) has nine reserved bits available for a covered channel. The explanations about the detectability are the same as for the PnP ID above.
	\item The PnP ID also features two octets for the Vendor ID, some values of which are not yet assigned to a vendor and some values of which are marked as “private”. The exact number of available values might and probably will change; however, a Bluetooth device would need either Internet access or an update to be able to resolve IDs that have been assigned after the shipment of the device's firmware to a vendor. Since “private” values cannot be linked to a specific vendor, they could be treated as unassigned and thus be used for the purposes of covert channels. The explanations about the detectability are the same as for the PnP ID above.
\end{itemize}

\subsection{Network State/Value Modulation}
This pattern is the overlying pattern to the one described in \Cref{sec:stego-reserved}. Its ID is \verb|E1n1.| as per \Cite{wendzel_GenericTaxonomySteganography_2022}. It describes the embedding of a covert message by modulating the state of a network element in general.
\begin{itemize}
	\item Inside the PnP ID (see \Cref{sec:pnp-id}), which is part of the GATT characteristics, there are two octets each for the Product ID and the Product Version. Both of these values are assigned by the manufacturer and are in most cases not publicly resolvable. Thus both values can be assigned at will; the covert channel will most likely not be detectable since the values and their assignment are not publicly accessible.
	\item Furthermore, most values of the GATT characteristics that devices usually expose can be changed without raising any suspicion. For instance, the \verb|Software Revision String| or the \verb|Firmware Revision String| both contain values that could only be checked for their plausibility if all possible values were publicly available. Since that is currently not the case and will certainly not be in the future, changing the values is possible without raising suspicions. The size of the channel depends on the number and type of characteristics that are altered.
	\item Additionally, further GATT characteristics that would otherwise be unused and the value of which can be arbitrarily assigned can be used to establish a covert channel. The detectability depends on the plausibility of the presence of a certain characteristic in a given context. For example, it would be rather conspicuous for a device that claims to be a blood oxygen sensor to feature characteristics that are normally only used by audio devices. The size of the covert channel also heavily depends on the number and type of characteristics added.
\end{itemize}
