% !TeX spellcheck = en_GB
\chapter{Introduction}
Since the beginning of its development in 1989, Bluetooth has become one of the most important standards for wireless communication~\cite{tuanc.nguyen_WhoInventedBluetooth_2021}. In 2020, there where over four billion devices capable of Bluetooth shipped worldwide in that year only. The annual shipments are growing by about 10 \% per year, so shipments are expected to reach 6.4 billion devices per year in 2025~\cite{bluetoothsiginc._2021BluetoothMarket_2021}.

As of late, due to the outbreak of the COVID-19 pandemic, Bluetooth – or rather Bluetooth Low Energy (BLE) – has taken a central role regarding the digital countermeasures. Currently, there are at least 38 countries worldwide that have implemented the “Google Apple Exposure Notification” (GAEN) framework\footnote{\url{https://developer.apple.com/exposure-notification/}}, which is the most widespread of the multiple solutions for digital contact tracing based on BLE. Countries that use the GAEN framework include at least 26 states of the USA, Canada, Russia, Germany, the UK, Japan, Saudi Arabia and many more~\cite{mishaalrahman_ListCountriesUsing_2021}. In Germany, the federal mobile application for digital contact tracing — the “Corona-Warn-App” — has been installed more than 40 million times, which further underlines the importance of BLE in that field~\cite{robertkochinstitute_OpenSourceProjectCoronaWarnApp_2022}.

All of the aforementioned facts make Bluetooth more and more attractive for bad actors to try and abuse the protocol in order to gather information about their victims or infiltrate their victims' devices. As of writing this thesis, there are no known tools for automatically scanning the environment to find Bluetooth-enabled devices and assess their vulnerability based on a large set of known vulnerabilities. The reasons for that are numerous:

On the one hand, the area of identifying technical device attributes of Bluetooth-enabled devices is not well researched so far. On the other hand, the amount of information provided by databases such as CVE and CVSS might not be sufficient to map devices to related vulnerabilities. Also, there are multiple vectors regarding Bluetooth that can make a device vulnerable. This thesis focuses on shedding some light on those aspects.

Nevertheless, the development of a tool capable of automatically assessing the vulnerability of a given device would be very useful for many applications such as penetration testing, research but potentially also to commit crimes.

\section{Research Questions, Research Gap and Relevance\label{sec:research-question}}

As with every other computer network, discovery of and knowledge about vulnerabilities of Bluetooth-enabled devices is crucial for the security of the network as a whole. Penetration testing of any computer system would not be complete without also considering Bluetooth as a point of entry, since more and more devices are relying on Bluetooth to serve their purpose. As of writing this thesis, there is no easy way to automatically test for Bluetooth-based vulnerabilities.

To shed some light on the reasons for that, this thesis breaks down the different steps that need to be taken in order to automatically identify vulnerabilities in Bluetooth-enabled devices. The focus points of this work will be:
\begin{itemize}
    \item Extraction of technical device properties
    \item Retrieval of information about vulnerabilities regarding the device
    \item Confirmation of the presence of retrieved vulnerabilities on the device
\end{itemize}
While working on the aforementioned focus points, the following information gaps have to be considered:
\begin{itemize}
    \item how to convert the technical device properties in such a way that they can be used as a suitable query for vulnerability databases
    \item how to transform information about vulnerabilities in such a way that they can be tested for presence on the device
\end{itemize}

Summarising the thoughts mentioned above, the thesis will focus on answering the following question:
\begin{questions}[start=0]
\item Is automated security-testing for Bluetooth-enabled devices practically feasible?\label{Q0}
\end{questions}

For answering this question, the following sub-questions covering the different focus points have to be solved:
\begin{questions}[resume]
\item What technical information can automatically and non-invasively be extracted from a given device?\label{Q1}
\item What information is needed to automatically resolve a device to a vulnerability or a set of vulnerabilities?\label{Q2}
\item How can the gap between the gathered and needed information be closed automatically?\label{Q3}\todo{Schreibweise im Rest der Arbeit anpassen}
\item How can it be evaluated automatically whether a potential vulnerability is present on a given device?\label{Q4}
\end{questions}
All of those sub-questions should be answered under the premises\label{premise} that no physical access to the devices is granted, that a large number of devices and vulnerabilities has to be covered and thus that the vulnerability assessment has to work automatically and without user interaction. All methods should be chosen in a way that no specialised hardware is necessary. \Cref{fig:problemstatement} gives an overview of the research topic, the information gaps and where the questions asked above are to be located.

\begin{figure}[H]
    \centering
    \includegraphics[width=\textwidth]{pictures/VisProblemstatement.pdf}
    \caption{Visualisation of the research topic, where the information gaps are located and which questions belong to which element of the testing procedure.}
    \label{fig:problemstatement}
\end{figure}

\section{Structure of the Thesis}
This thesis will be structured as follows: \Cref{ch:background} explains important technical concepts and gives other information necessary for the understanding of this thesis. Based on that, \Cref{ch:related_work} will focus on scientific research on similar topics. It will be described which solutions the authors have come up with and where the connections to the topic of this thesis are. \Cref{ch:methodology} focuses on the conception of the automated security-testing. Different alternatives are presented and discussed thoroughly. A concept for the realisation of the concept based on the alternatives is developed and described. The results are presented and discussed in \Cref{ch:evaluation} and concluded in \Cref{ch:conclusion}, also pointing out possible directions for future work.